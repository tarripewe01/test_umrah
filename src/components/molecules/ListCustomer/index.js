import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Card, Container, Form, Table } from "react-bootstrap";
import { API_URL } from "../../../helper/API_URL";

const ListCustomer = () => {
  const getAllData = async () => {
    const token = localStorage.getItem("token");
    const config = { headers: { Authorization: token } };
    await axios
      .get(`${API_URL}/customer/list/10/1`, config)
      .then(function (response) {
        setListTable(response.data.data.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const onRefresh = (e) => {
    e.preventDefault();
    getAllData();
    setSearch("");
  };

  //  Search
  const [search, setSearch] = useState("");
  const handleSearchChange = (e) => {
    setSearch(e.target.value);
  };
  const searchData = async () => {
    const token = localStorage.getItem("token");
    const config = { headers: { Authorization: token } };
    await axios
      .get(`${API_URL}/customer/list/10/1?keywords=${search}`, config)
      .then(function (response) {
        setListTable(response.data.data.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  // All Data
  const [listTable, setListTable] = useState([]);
  useEffect(async () => {
    getAllData();
  }, []);

  return (
    <Container>
      <p style={styles.Title}>List Sales</p>
      <Card style={styles.Contain}>
        <div>
          <Button onClick={onRefresh} style={styles.BtnRefresh}>
            Refresh
          </Button>
        </div>
        <Form>
          <Form.Group
            className="mb-3 mt-3"
            controlId="exampleForm.ControlInput1"
          >
            <Form.Label>Search</Form.Label>
            <Form.Control
              type="search"
              placeholder="Search"
              value={search}
              onChange={handleSearchChange}
            />
          </Form.Group>
        </Form>
        <div style={{ textAlign: "end" }}>
          <Button style={styles.BtnSearch} onClick={searchData}>
            Search
          </Button>
        </div>
      </Card>

      <Table bordered style={styles.ContainList}>
        <thead>
          <tr>
            <th>No</th>
            <th>Customer ID</th>
            <th>Customer Name</th>
            <th>Contact</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>
          {listTable.map((customer, index) => (
            <tr key={index}>
              <td>{index + 1}</td>
              <td>{customer.id_customer}</td>
              <td>{customer.nama}</td>
              <td>{customer.contact}</td>
              <td>{customer.alamat}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Container>
  );
};

export default ListCustomer;

const styles = {
  Title: { fontSize: 24, color: "#3F3F3F", marginTop: 15 },
  Contain: {
    paddingTop: 18,
    paddingBottom: 18,
    paddingLeft: 24,
    paddingRight: 24,
  },
  BtnRefresh: {
    width: 110,
    backgroundColor: "#7AD6F3",
    borderColor: "#7AD6F3",
  },
  BtnSearch: {
    backgroundColor: "#3898EC",
    borderColor: "#3898EC",
    width: 110,
  },
  ContainList: {
    textAlign: "center",
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center",
  },
};
