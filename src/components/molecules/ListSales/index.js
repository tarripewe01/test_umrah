import React, { useState, useEffect } from "react";
import { Button, Card, Container, Form, Table } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const ListSales = () => {
  const navigate = useNavigate();

  const getAllData = async () => {
    const token = localStorage.getItem("token");
    const config = { headers: { Authorization: token } };
    await axios
      .get("https://api.umrah.ac.id/api/transaction/list/10/1", config)
      .then(function (response) {
        setListTable(response.data.data.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const onCreate = (e) => {
    e.preventDefault();
    navigate("/new-sales");
  };
  const onRefresh = (e) => {
    e.preventDefault();
    getAllData();
    setSearch("");
  };

  const [search, setSearch] = useState("");
  const handleSearchChange = (e) => {
    setSearch(e.target.value);
  };
  const searchData = async () => {
    const token = localStorage.getItem("token");
    const config = { headers: { Authorization: token } };
    await axios
      .get(
        `https://api.umrah.ac.id/api/transaction/list/10/1?keywords=${search}`,
        config
      )
      .then(function (response) {
        setListTable(response.data.data.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const [listTable, setListTable] = useState([]);

  useEffect(() => {
    getAllData();
  }, []);

  return (
    <Container>
      <p style={styles.Title}>List Sales</p>
      <Card style={styles.Contain}>
        <div>
          <Button onClick={onCreate} style={styles.BtnNew}>
            New
          </Button>
          <Button onClick={onRefresh} style={styles.BtnRefresh}>
            Refresh
          </Button>
        </div>
        <Form>
          <Form.Group
            className="mb-3 mt-3"
            controlId="exampleForm.ControlInput1"
          >
            <Form.Label>Search</Form.Label>
            <Form.Control
              type="search"
              placeholder="Search"
              value={search}
              onChange={handleSearchChange}
            />
          </Form.Group>
        </Form>
        <div style={{ textAlign: "end" }}>
          <Button style={styles.BtnSearch} onClick={searchData}>
            Search
          </Button>
        </div>
      </Card>

      <Table bordered style={styles.ContainList}>
        <thead>
          <tr>
            <th>No</th>
            <th>Action</th>
            <th>Transaction Code</th>
            <th>Customer</th>
            <th>Transaction Date</th>
            <th>Grand Total</th>
          </tr>
        </thead>
        <tbody>
          {listTable.map((sales, index) => (
            <tr key={index}>
              <td>{index + 1}</td>
              <div style={styles.ContainBtn}>
                <Button style={styles.BtnDetail}>Detail</Button>
                <Button style={styles.BtnDelete}>Delete</Button>
              </div>
              <td>{sales.id_transaksi}</td>
              <td>{sales.id_customer}</td>
              <td>13 Desember 2021 - 11:05:42</td>
              <td>Rp.{sales.total_bayar}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Container>
  );
};

export default ListSales;

const styles = {
  Title: { fontSize: 24, color: "#3F3F3F", marginTop: 15 },
  Contain: {
    //   backgroundColor: "red",
    paddingTop: 18,
    paddingBottom: 18,
    paddingLeft: 24,
    paddingRight: 24,
  },
  BtnNew: {
    width: 110,
    backgroundColor: "#3898EC",
    borderColor: "#3898EC",
  },
  BtnRefresh: {
    width: 110,
    backgroundColor: "#7AD6F3",
    borderColor: "#7AD6F3",
    marginLeft: 15,
  },
  BtnSearch: {
    backgroundColor: "#3898EC",
    borderColor: "#3898EC",
    width: 110,
  },
  ContainList: {
    textAlign: "center",
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  ContainBtn: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  BtnDetail: {
    width: 110,
    backgroundColor: "#3898EC",
    borderColor: "#3898EC",
  },
  BtnDelete: {
    width: 110,
    backgroundColor: "#F58383",
    borderColor: "#F58383",
    marginLeft: 5,
  },
};
