import React from "react";
import { Container, Nav } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

const Header = () => {
  const navigate = useNavigate();
  const token = localStorage.getItem("token");

  const SignOut = () => {
    localStorage.removeItem("token");
    navigate("/");
  };
  return (
    <div style={{ backgroundColor: "#F3F3F3", paddingBottom: 18 }}>
      <Container className="pt-3">
        <Nav
          variant="pills"
          defaultActiveKey="/sales"
          className="justify-content-end"
        >
          <Nav.Item>
            <Nav.Link href="/sales">Sales</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="2" href="/customer">
              Customer
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="3" href="/item">
              Item
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link onClick={SignOut} eventKey="4">
              Signout
            </Nav.Link>
          </Nav.Item>
        </Nav>
      </Container>
    </div>
  );
};

export default Header;
