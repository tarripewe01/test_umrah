import axios from "axios";
import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  Container,
  Form,
  Image,
  Modal,
  Table,
} from "react-bootstrap";
import { API_URL } from "../../../helper/API_URL";

const ListItem = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const getAllData = async () => {
    const token = localStorage.getItem("token");
    const config = { headers: { Authorization: token } };
    await axios
      .get(`${API_URL}/item/list/10/1`, config)
      .then(function (response) {
        setListTable(response.data.data.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const onRefresh = async (e) => {
    e.preventDefault();
    getAllData();
    setSearch("");
  };

  const [search, setSearch] = useState("");
  const handleSearchChange = (e) => {
    setSearch(e.target.value);
  };
  const searchData = async () => {
    const token = localStorage.getItem("token");
    const config = { headers: { Authorization: token } };
    await axios
      .get(`${API_URL}/item/list/10/1?keywords=${search}`, config)
      .then(function (response) {
        setListTable(response.data.data.data);
      })
      .catch(function (error) {
        console.log(error);
      });
    
  };

  const [listTable, setListTable] = useState([]);
  useEffect(async () => {
    getAllData();
  }, []);

  return (
    <Container>
      <p style={styles.Title}>Item</p>
      <Card style={styles.Contain}>
        <div>
          <Button onClick={onRefresh} style={styles.BtnRefresh}>
            Refresh
          </Button>
        </div>
        <Form>
          <Form.Group
            className="mb-3 mt-3"
            controlId="exampleForm.ControlInput1"
          >
            <Form.Label>Search</Form.Label>
            <Form.Control
              type="search"
              placeholder="Search"
              value={search}
              onChange={handleSearchChange}
              onClick={handleShow}
            />
          </Form.Group>
        </Form>
        <div style={{ textAlign: "end" }}>
          <Button style={styles.BtnSearch} onClick={searchData}>
            Search
          </Button>
        </div>
      </Card>

      <Modal show={show} onHide={handleClose} size="xl">
        <Modal.Header closeButton>
          <Modal.Title>Search Item</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group
              className="mb-3 mt-3"
              controlId="exampleForm.ControlInput1"
            >
              <Form.Control
                type="search"
                placeholder="Search"
                onClick={handleShow}
                onChange={handleSearchChange}
              />
            </Form.Group>
          </Form>
          <Table bordered style={styles.ContainList}>
            <thead>
              <tr>
                <th>No</th>
                <th>Action</th>
                <th>Item ID</th>
                <th>Item Name</th>
                <th>Unit Price</th>
              </tr>
            </thead>
            <tbody>
              {listTable.map((item, index) => (
                <tr>
                  <td>{index + 1}</td>
                  <div style={styles.ContainBtn}>
                    <Button
                      onClick={handleClose}
                      style={{
                        width: 80,
                        backgroundColor: "#3898EC",
                        borderColor: "#3898EC",
                      }}
                    >
                      Choose
                    </Button>
                  </div>
                  <td>{item.kode_item}</td>
                  <td>{item.nama_item}</td>
                  <td>Rp. {item.harga_satuan},00</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={searchData}>
            Search
          </Button>
        </Modal.Footer>
      </Modal>

      <Table bordered style={styles.ContainList}>
        <thead>
          <tr>
            <th>No</th>
            <th>Item Name</th>
            <th>Unit</th>
            <th>Stock</th>
            <th>Unit Price</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {listTable.map((item, index) => (
            <tr key={index}>
              <td>{index + 1}</td>
              <td>{item.nama_item}</td>
              <td>{item.unit}</td>
              <td>{item.stok}</td>
              <td>Rp. {item.harga_satuan},00</td>
              <td>
                <Image
                  style={styles.Image}
                  src={`https://api.umrah.ac.id/${item.barang}`}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Container>
  );
};

export default ListItem;

const styles = {
  Title: { fontSize: 24, color: "#3F3F3F", marginTop: 15 },
  Contain: {
    paddingTop: 18,
    paddingBottom: 18,
    paddingLeft: 24,
    paddingRight: 24,
  },
  BtnRefresh: {
    width: 110,
    backgroundColor: "#7AD6F3",
    borderColor: "#7AD6F3",
  },
  BtnSearch: {
    backgroundColor: "#3898EC",
    borderColor: "#3898EC",
    width: 110,
  },
  ContainList: {
    textAlign: "center",
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  Image: { width: 80, height: 70 },
};
