import React, { useState } from "react";
import { Button, Container, Form, Modal, Table } from "react-bootstrap";

const TableNewSales = () => {
  const [qty, setQty] = useState("");
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <Container>
      <Table bordered size="lg" style={{ textAlign: "center" }}>
        <thead>
          <>
            <th>No</th>
            <th>Action</th>
            <th>Item ID</th>
            <th>Item Name</th>
            <th>Qty</th>
            <th>Grand Total</th>
          </>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <div>
              <Button
                style={{
                  backgroundColor: "#F58383",
                  borderColor: "#F58383",
                  width: 110,
                  height: 35,
                  fontSize: 15,
                }}
              >
                Delete
              </Button>
            </div>
            <td>123456789</td>
            <td>Kopi Gula Aren</td>
            <div>
              <input
                style={{
                  borderRadius: 10,
                  width: 70,
                  height: 35,
                  borderColor: "#3F3F3F",
                  textAlign: "center",
                  borderWidth: 1,
                }}
                value={qty}
                onChange={(e) => setQty(e.target.value)}
              />
            </div>
            <td>Rp. 24000,00</td>
          </tr>
          <tr>
            <td>2</td>
            <div>
              <Button
                style={{
                  backgroundColor: "#F58383",
                  borderColor: "#F58383",
                  width: 110,
                  height: 35,
                  fontSize: 15,
                }}
              >
                Delete
              </Button>
            </div>
            <td>123456789</td>
            <td>Kopi Gula Aren</td>
            <div>
              <input
                style={{
                  borderRadius: 10,
                  width: 70,
                  height: 35,
                  borderColor: "#3F3F3F",
                  textAlign: "center",
                  borderWidth: 1,
                }}
                value={qty}
                onChange={(e) => setQty(e.target.value)}
              />
            </div>
            <td>Rp. 24000,00</td>
          </tr>
        </tbody>
      </Table>
      <div
        style={{
          textAlign: "end",
        }}
      >
        <Button
          onClick={handleShow}
          style={{
            backgroundColor: "#F1C40E",
            borderColor: "#F1C40E",
          }}
        >
          Pay Now
        </Button>

        <Modal show={show} onHide={handleClose} className="shadow-lg" size="lg">
          <Modal.Header closeButton>
            <Modal.Title style={{ fontSize: 24 }}>
              Payment Transaction
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label style={{ fontSize: 20 }}>Customer</Form.Label>
                <Form.Control
                  type="customer"
                  style={{ backgroundColor: "#F3F3F3" }}
                />
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label style={{ fontSize: 20 }}>Grand Total</Form.Label>
                <Form.Control
                  type="grandtotal"
                  style={{ backgroundColor: "#F3F3F3" }}
                />
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label style={{ fontSize: 20 }}>Total</Form.Label>
                <Form.Control type="grandtotal" />
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label style={{ fontSize: 20 }}>Exchange</Form.Label>
                <Form.Control
                  type="grandtotal"
                  style={{ backgroundColor: "#F3F3F3" }}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <div
            style={{
              textAlign: "end",
              marginTop: 100,
              marginBottom: 50,
              marginRight: 20,
            }}
          >
            <Button
              variant="secondary"
              onClick={handleClose}
              style={{
                backgroundColor: "#B0B0B0",
                borderColor: "#B0B0B0",
                width: 100,
              }}
            >
              Close
            </Button>
            <Button
              variant="primary"
              onClick={handleClose}
              style={{
                backgroundColor: "#62B13D",
                borderColor: "#62B13D",
                width: 100,
                marginLeft: 15,
              }}
            >
              Pay
            </Button>
          </div>
        </Modal>
      </div>
    </Container>
  );
};

export default TableNewSales;
