import axios from "axios";
import React, { useEffect, useState, useCallback } from "react";
import { Button, Card, Container, Form, Modal, Table } from "react-bootstrap";
import { API_URL } from "../../../helper/API_URL";
import _debounce from "lodash/debounce";

const NewSalesForm = () => {
  const [qty, setQty] = useState("");
  const [show, setShow] = useState(false);
  const [listTableCustomer, setListTableCustomer] = useState([]);
  // const [listTableItem, setListTableItem] = useState([]);
  const [search, setSearch] = useState("Beras");
  const [item, setItem] = useState([]);

  let listTableItem = [];

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  const handleSearchChange = (e) => {
    setSearch(e.target.value);
    console.log(e.target.value, 'search');
  };

  const getAllDataCustomer = async () => {
    const token = localStorage.getItem("token");
    const config = { headers: { Authorization: token } };
    await axios
      .get(`${API_URL}/customer/list/10/1`, config)
      .then(function (response) {
        setListTableCustomer(response.data.data.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const getAllDataItem = async () => {
    const token = localStorage.getItem("token");
    const config = { headers: { Authorization: token } };
    await axios
      .get(`${API_URL}/item/list/10/1`, config)
      .then(function (response) {
        alert("setTable");
        listTableItem = [response.data.data.data];
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  // const searchData = async () => {
  //   const token = localStorage.getItem("token");
  //   const config = { headers: { Authorization: token } };
  //   await axios
  //     .get(`${API_URL}/item/list/10/1?keywords=${search}`, config)
  //     .then(function (response) {
  //       setListTableItem(response.data.data.data);
  //     })
  //     .catch(function (error) {
  //       console.log(error);
  //     });
  // };

  useEffect(() => {
    getAllDataCustomer();
    getAllDataItem();
  }, []);

  // const setNameCustomer = (val) => {
  //   localStorage.setItem("nameCustomer", val);
  // };

  console.log(listTableItem);
  const changeHandler = () => {
    getAllDataItem();
    console.log("search event", listTableItem);

    const split = search.split("+");
    const arr = [...listTableItem[0]];
    const find = arr.find((val) => val.kode_item === split[0]);
    alert("salah");
    console.log(find, "ini hasil find", arr, search);
    setItem([
      ...item,
      {
        action: "1",
        itemId: split[0],
        itemName: find.nama_item,
        qty: split[1],
        grandTotal: parseInt(split[1]) * find.harga_satuan,
      },
    ]);
  };

  const debouncedChangeHandler = useCallback(_debounce(changeHandler, 0), []);

  return (
    <Container>
      <p style={style.Title}>New Sales Transaction</p>
      <Form>
        <Card style={style.Card}>
          <fieldset>
            <p style={style.Subtitle}>Customer</p>
            <Form.Group className="mb-3">
              <Form.Select
                style={style.Option}
                // onChange={(e) => setNameCustomer(e.target.value)}
              >
                <option>Choose Customer</option>
                {listTableCustomer.map((customer, index) => (
                  <>
                    <option value={customer.nama} key={index}>
                      {customer.nama}
                    </option>
                  </>
                ))}
              </Form.Select>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Control
                type="search"
                placeholder="Text"
                onChange={(e) => handleSearchChange(e)}
                value={search}
              />
            </Form.Group>
            <div style={{ textAlign: "end" }}>
              <Button
                className="shadow-lg"
                style={style.Search}
                type="submit"
                // onClick={searchData}
              >
                Search
              </Button>
              <Button
                onClick={(e) => debouncedChangeHandler(e)}
                className="shadow-lg"
                style={style.Add}
                type="button"
              >
                Add Item
              </Button>
            </div>
          </fieldset>
        </Card>
      </Form>

      <Table bordered size="lg" style={{ textAlign: "center" }}>
        <thead>
          <>
            <th>No</th>
            <th>Action</th>
            <th>Item ID</th>
            <th>Item Name</th>
            <th>Qty</th>
            <th>Grand Total</th>
          </>
        </thead>
        <tbody>
          {item.map((all, i) => (
            <tr>
              <td>{i + 1}</td>
              <div>
                <Button style={style.Delete}>Delete</Button>
              </div>
              <td>{all.itemId}</td>
              <td>{all.itemName}</td>
              <div>
                <input
                  style={style.inputQty}
                  value={all.qty}
                  onChange={(e) => setQty(e.target.value)}
                />
              </div>
              <td>Rp. {all.grandTotal},00</td>
            </tr>
          ))}
        </tbody>
      </Table>
      <div
        style={{
          textAlign: "end",
        }}
      >
        <Button onClick={handleShow} style={style.Pay}>
          Pay Now
        </Button>

        <Modal show={show} onHide={handleClose} className="shadow-lg" size="lg">
          <Modal.Header closeButton>
            <Modal.Title style={{ fontSize: 24 }}>
              Payment Transaction
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label style={style.TitleModal}>Customer</Form.Label>
                <Form.Control
                  type="customer"
                  style={style.Form}
                  // value={localStorage.getItem("nameCustomer")}
                />
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label style={style.TitleModal}>Grand Total</Form.Label>
                <Form.Control type="grandtotal" style={style.Form} />
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label style={style.TitleModal}>Total</Form.Label>
                <Form.Control type="grandtotal" />
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label style={style.TitleModal}>Exchange</Form.Label>
                <Form.Control type="grandtotal" style={style.Form} />
              </Form.Group>
            </Form>
          </Modal.Body>
          <div
            style={{
              textAlign: "end",
              marginTop: 100,
              marginBottom: 50,
              marginRight: 20,
            }}
          >
            <Button
              variant="secondary"
              onClick={handleClose}
              style={{
                backgroundColor: "#B0B0B0",
                borderColor: "#B0B0B0",
                width: 100,
              }}
            >
              Close
            </Button>
            <Button
              variant="primary"
              onClick={handleClose}
              style={{
                backgroundColor: "#62B13D",
                borderColor: "#62B13D",
                width: 100,
                marginLeft: 15,
              }}
            >
              Pay
            </Button>
          </div>
        </Modal>
      </div>
    </Container>
  );
};

export default NewSalesForm;

const style = {
  Title: { marginTop: 15, fontSize: 24 },
  Card: {
    paddingTop: 18,
    paddingBottom: 18,
    paddingLeft: 24,
    paddingRight: 24,
    marginBottom: 20,
  },
  Subtitle: { fontSize: 24 },
  Option: { color: "#B0B0B0" },
  Search: {
    width: 118,
    height: 40,
    backgroundColor: "#3898EC",
    borderColor: "#3898EC",
  },
  Add: {
    width: 118,
    height: 40,
    marginLeft: 22,
    backgroundColor: "#62B13D",
    borderColor: "#62B13D",
  },
  Delete: {
    backgroundColor: "#F58383",
    borderColor: "#F58383",
    width: 110,
    height: 35,
    fontSize: 15,
  },
  Qty: {
    borderRadius: 10,
    width: 70,
    height: 35,
    borderColor: "#3F3F3F",
    textAlign: "center",
    borderWidth: 1,
  },
  Pay: {
    backgroundColor: "#F1C40E",
    borderColor: "#F1C40E",
  },
  TitleModal: { fontSize: 20 },
  Form: { backgroundColor: "#F3F3F3" },
  inputQty: {
    borderRadius: 10,
    width: 70,
    height: 35,
    borderColor: "#3F3F3F",
    textAlign: "center",
    borderWidth: 1,
  },
};
