import "../App.css";
import Login from "./Login";
import Sales from "./Sales";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import NewSales from "../pages/NewSales";
import Customer from "./Customer";
import Item from "./Item";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Login />} />
        <Route path="sales" element={<Sales />} />
        <Route path="/new-sales" element={<NewSales />} />
        <Route path="/customer" element={<Customer />} />
        <Route path="/item" element={<Item />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
