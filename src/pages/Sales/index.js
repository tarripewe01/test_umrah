import React from "react";
import Header from "../../components/molecules/Header";
import ListSales from "../../components/molecules/ListSales";

const Sales = () => {
  return (
    <>
      <Header />
      <ListSales />
    </>
  );
};

export default Sales;
