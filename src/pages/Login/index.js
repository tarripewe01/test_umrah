import React, { useState } from "react";
import { Card, Container, Form, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import "./style.css";

const Login = () => {
  const navigate = useNavigate();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
  };
  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const onLogin = async () => {
    console.log(username, password);
    let item = { username, password };

    let result = await axios
      .post("https://api.umrah.ac.id/api/token", {
        username: username,
        password: password,
      })
      .then(function (response) {
        console.log(response.data);
        localStorage.setItem("token", `Bearer ${response.data.token}`);
        navigate("/sales");
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  return (
    <Container style={styles.Container} className="d-grid justify-center">
      <Card style={styles.Card} className="shadow-lg">
        <Form className="px-5 py-3 ">
          <h5 style={styles.Title}>Login</h5>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label style={styles.Label}>Username</Form.Label>
            <Form.Control
              style={styles.Control}
              type="Username"
              placeholder="Username"
              value={username}
              onChange={handleUsernameChange}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label style={styles.Label}>Password</Form.Label>
            <Form.Control
              style={styles.Control}
              type="Password"
              placeholder="Password"
              value={password}
              onChange={handlePasswordChange}
            />
          </Form.Group>
          <div style={styles.ButtonContain}>
            <Button
              onClick={onLogin}
              style={styles.Button}
              className="shadow-lg"
              variant="primary"
            >
              Login
            </Button>
          </div>
        </Form>
      </Card>
    </Container>
  );
};

export default Login;

const styles = {
  Container: {
    alignItems: "center",
    placeItems: "center",
    height: "100vh",
  },
  Card: {
    justifyContent: "center",
    width: 627,
    height: 528,
  },
  Title: {
    fontSize: 24,
    marginBottom: 24,
    color: "#3F3F3F",
  },
  Label: { fontSize: 20 },
  Control: { fontSize: 14, backgroundColor: "#EBEDEE" },
  ButtonContain: { textAlign: "end" },
  Button: {
    backgroundColor: "#F1C40E",
    width: 118,
    borderColor: "#F1C40E",
    marginTop: 62,
  },
};
