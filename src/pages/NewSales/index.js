import React from "react";
import Header from "../../components/molecules/Header";
import NewSalesForm from "../../components/molecules/NewSalesForm";

const NewSales = () => {
  return (
    <>
      <Header />
      <NewSalesForm />
    </>
  );
};

export default NewSales;
