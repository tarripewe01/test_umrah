import React from "react";
import Header from "../../components/molecules/Header";
import ListItem from "../../components/molecules/ListItem";

const Item = () => {
  return (
    <>
      <Header />
      <ListItem />
    </>
  );
};

export default Item;
