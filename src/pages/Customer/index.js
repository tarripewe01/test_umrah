import React from "react";
import Header from "../../components/molecules/Header";
import ListCustomer from "../../components/molecules/ListCustomer";

const Customer = () => {
  return (
    <>
      <Header />
      <ListCustomer />
    </>
  );
};

export default Customer;
